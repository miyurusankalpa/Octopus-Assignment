provider "aws" {
  region = "ap-southeast-1"

  default_tags {
   tags = {
     Project     = "OctopusBI-TEST"
   }
  }
}

module "sg-webserver_cluster" {
  source = "../modules/instance"
}

output "region" {
    value = "${module.sg-webserver_cluster.region_id}"
}
output "instance_1" {
    value = "${module.sg-webserver_cluster.instance_1_id}"
}
output "instance_2" {
    value = "${module.sg-webserver_cluster.instance_2_id}"
}
