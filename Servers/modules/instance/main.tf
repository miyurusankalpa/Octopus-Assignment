data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "debian" {
  owners = ["136693071363"]
  most_recent = true
	
  filter {
    name   = "name"
    values = ["debian-12-arm64*"]
  }
}

resource "aws_security_group" "instance_sg" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"

  ingress {
    description      = "HTTP From AC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks =  ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_instance" "web_1" {
  tags = {
    Name = "web1"
  }

  instance_type          = "t4g.micro"
  vpc_security_group_ids = [aws_security_group.instance_sg.id]
  availability_zone      = data.aws_availability_zones.available.names[0]

  ami       = data.aws_ami.debian.id
  user_data = "${file("../modules/instance/userdata.sh")}"
}

resource "aws_instance" "web_2" {
  tags = {
    Name = "web2"
  }

  instance_type          = "t4g.micro"
  vpc_security_group_ids = [aws_security_group.instance_sg.id]
  availability_zone      = data.aws_availability_zones.available.names[1]

  ami       = data.aws_ami.debian.id
  user_data = "${file("../modules/instance/userdata.sh")}"
}
