output "region_id" {
  value = data.aws_availability_zones.available.id
}

output "instance_1_id" {
  value = aws_instance.web_1.id
}

output "instance_2_id" {
  value = aws_instance.web_2.id
}
