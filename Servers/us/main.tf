provider "aws" {
  region = "us-east-2"

  default_tags {
   tags = {
     Project     = "OctopusBI-TEST"
   }
  }
}

module "us-webserver_cluster" {
  source = "../modules/instance"
}

output "region" {
    value = "${module.us-webserver_cluster.region_id}"
}
output "instance_1" {
    value = "${module.us-webserver_cluster.instance_1_id}"
}
output "instance_2" {
    value = "${module.us-webserver_cluster.instance_2_id}"
}
