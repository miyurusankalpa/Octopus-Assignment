provider "aws" {
  region = "eu-west-1"

  default_tags {
   tags = {
     Project     = "OctopusBI-TEST"
   }
  }
}

module "ie-webserver_cluster" {
  source = "../modules/instance"
}

output "region" {
    value = "${module.ie-webserver_cluster.region_id}"
}
output "instance_1" {
    value = "${module.ie-webserver_cluster.instance_1_id}"
}
output "instance_2" {
    value = "${module.ie-webserver_cluster.instance_2_id}"
}
