provider "aws" {
  region = "us-west-2"

  default_tags {
   tags = {
     Project     = "OctopusBI-TEST"
   }
  }
}

resource "aws_globalaccelerator_accelerator" "test" {
  name            = "OctopusBI-TEST"
  ip_address_type = "IPV4"
  enabled         = true
}

resource "aws_globalaccelerator_listener" "http" {
  accelerator_arn = aws_globalaccelerator_accelerator.test.id
  client_affinity = "SOURCE_IP"
  protocol        = "TCP"

  port_range {
    from_port = 80
    to_port   = 80
  }
}

#US endpoint group
data "terraform_remote_state" "usweb" {
  backend = "local"

  config = {
    path = "../Servers/us/terraform.tfstate"
  }
}

resource "aws_globalaccelerator_endpoint_group" "us-group" {
  listener_arn = aws_globalaccelerator_listener.http.id
  endpoint_group_region = data.terraform_remote_state.usweb.outputs.region

  endpoint_configuration {
    client_ip_preservation_enabled = true
    endpoint_id = data.terraform_remote_state.usweb.outputs.instance_1
    weight      = 50
  }
  endpoint_configuration {
    client_ip_preservation_enabled = true
    endpoint_id = data.terraform_remote_state.usweb.outputs.instance_2
    weight      = 50
  }
}

#EU endpoint group
data "terraform_remote_state" "euweb" {
  backend = "local"

  config = {
    path = "../Servers/eu/terraform.tfstate"
  }
}

resource "aws_globalaccelerator_endpoint_group" "eu-group" {
  listener_arn = aws_globalaccelerator_listener.http.id
  endpoint_group_region = data.terraform_remote_state.euweb.outputs.region

  endpoint_configuration {
    client_ip_preservation_enabled = true
    endpoint_id = data.terraform_remote_state.euweb.outputs.instance_1
    weight      = 50
  }
  endpoint_configuration {
    client_ip_preservation_enabled = true
    endpoint_id = data.terraform_remote_state.euweb.outputs.instance_2
    weight      = 50
  }
}

#AP endpoint group
data "terraform_remote_state" "apweb" {
  backend = "local"

  config = {
    path = "../Servers/ap/terraform.tfstate"
  }
}

resource "aws_globalaccelerator_endpoint_group" "ap-group" {
  listener_arn = aws_globalaccelerator_listener.http.id
  endpoint_group_region = data.terraform_remote_state.apweb.outputs.region

  endpoint_configuration {
    client_ip_preservation_enabled = true
    endpoint_id = data.terraform_remote_state.apweb.outputs.instance_1
    weight      = 50
  }
  endpoint_configuration {
    client_ip_preservation_enabled = true
    endpoint_id = data.terraform_remote_state.apweb.outputs.instance_2
    weight      = 50
  }
}

output "url" {
  value = aws_globalaccelerator_accelerator.test.dns_name
}

