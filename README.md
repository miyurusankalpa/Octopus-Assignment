# Multi-region application deployment
This architecture involves deploying a web application across multiple AWS regions for
improved availability and reduced latency.

## Creates

* 1 AWS Global Accelerator
* 6 t4g-nano Servers
* 6 Security Groups

## Folder Structure

```
├── create.sh
├── destroy.sh
├── GlobalAccelerator
│   ├── main.tf
│   ├── terraform.tfstate
│   └── terraform.tfstate.backup
├── README.md                       <-- You are here
├── Servers
│   ├── ap
│   │   ├── main.tf
│   │   ├── terraform.tfstate
│   │   └── terraform.tfstate.backup
│   ├── eu
│   │   ├── main.tf
│   │   ├── terraform.tfstate
│   │   └── terraform.tfstate.backup
│   ├── modules
│   │   └── instance
│   │       ├── main.tf
│   │       ├── outputs.tf
│   │       └── userdata.sh
│   └── us
│       ├── main.tf
│       ├── terraform.tfstate
│       └── terraform.tfstate.backup
└── terraform.tfstate

8 directories, 19 files
```

## Run

Set the profile

`export AWS_PROFILE=`

Run `create.sh` to deploy the infrastructure.

Run `destroy.sh` to remove the infrastructure.

## Adding a new region

1. Copy a region from the `Servers` folder and rename with the 2 letter region code.
2. Replace the region paramter on the aws provider in the folder.
3. Rename the resources to match the region code.
4. Add the new region folder to the both bash files.

## Demo 

https://youtu.be/hbeUy8JtZT4
