#!/bin/bash

#fail on error
set -e

echo "Create 1 servers 2 availability zones in 3 regions"

read -p "Press enter to continue"

cd Servers

cd us

terraform init

terraform apply

cd ../eu

terraform init

terraform apply

cd ../ap

terraform init

terraform apply

echo -e "\n"

echo "Create the Global Accelerator"

read -p "Press enter to continue"

cd ../../GlobalAccelerator

terraform init

terraform apply
