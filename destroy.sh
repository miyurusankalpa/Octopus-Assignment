#!/bin/bash

echo "Remove the Global Accelerator"

read -p "Press enter to continue"

cd GlobalAccelerator

terraform destroy

echo "Remove the servers"

read -p "Press enter to continue"

cd ../Servers

cd us

terraform destroy

cd ../eu

terraform destroy

cd ../ap

terraform destroy
